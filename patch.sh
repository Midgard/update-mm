#!/bin/bash

base="$1"
if [ -z "$base" ]; then base="."; fi
dist="$base/client"

if [ ! -d "$dist" ]; then
	echo "No Mattermost installation recognized. Pass the location of the Mattermost root as the first argument, or CD there."
	exit 1
fi

grep -q 'h1.markdown__heading .emoticon{min-height:128px;min-width:128px}' "$dist"/main.*css || \
sed -i 's/}.markdown__heading .emoticon{/}h1.markdown__heading .emoticon{min-height:128px;min-width:128px&/' "$dist"/main.*css

if ! grep -q 'h1.markdown__heading .emoticon{min-height:128px;min-width:128px}' "$dist"/main.*css; then echo "Failed!"; fi

# Disable YouTube preview images
find "$dist" -name "*.js" -o -name "*.js.map" | xargs sed -ri '
	s|(youtu\\?\.?be)|youpoop|g
	s|"https://i.ytimg.com/vi/"+[^"]*+"/[^"]*"|"foo"|g
	s|(['\''"])[^'\''"]*ytimg\.com(['\''"])|\1foo?ignore=\2|g
'
