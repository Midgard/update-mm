# Update Mattermost

Script to simplify updating Mattermost.

Puts different versions in a directory `$MATTERMOST_INSTALL_DIR` (default /opt/mattermost) and symlinks the most recently installed version to `$MATTERMOST_INSTALL_DIR/mattermost-current`.

Make sure to put all content you want to keep (the config, uploads and plugins) *outside* of the installation directory, otherwise they will not be used in the new version. This is not a bug, it's a feature. When you have multiple versions, things get messy if those are kept in the installation directory.

Make sure to update your config file once in a while. The author does this by, after each upgrade, diffing the previous factory file against the current factory file (e.g. `git diff --no-index /opt/mattermost/mattermost-5.{2,3}.1-team/config/config.json`) and adding the changed options to their config file in `/etc/mattermost.conf.json`.
